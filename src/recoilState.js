import { atom } from "recoil";

export const playState = atom({
    key: 'playState', // unique ID (with respect to other atoms/selectors)
    default: true, // default value (aka initial value)
  });