import React, { useEffect, useRef, useState } from "react";
import { useSpring, animated } from "@react-spring/web";
import { duration } from "./CssAnimations";
import { useRecoilValue } from "recoil";
import { playState } from "./recoilState";
const { coordinates } = require("./coordinates.json");
const { rectangles } = require("./rectangles.json");

const SpringAnimateItem = (props) => {
  const { circleCoordinates, rectCoordinates = [], color } = props;
  const [index, setIndex] = useState(1);
  const timeout_ref = useRef();
  const isPlaying = useRecoilValue(playState);
  const [animatedProps, api] = useSpring(() => ({
    config: { friction: 30 },
    from: {
      cx: circleCoordinates[0].x,
      cy: circleCoordinates[0].y,
      d: rectCoordinates.length ? rectCoordinates[0] : "",
    },
    to: circleCoordinates.slice(1).map((coord, ind) => ({
      cx: coord.x,
      cy: coord.y,
      d: rectCoordinates[ind],
    })),
  }));
  useEffect(() => {
    isPlaying ? api.resume() : api.pause();
  }, [isPlaying]);

  //   useEffect(() => {
  //     if (index >= circleCoordinates.length) {
  //       setIndex(1);
  //     }
  //   }, [index]);

  //   useEffect(() => {
  //     if (index < circleCoordinates.length) {
  //       api.start({
  //         cx: circleCoordinates[index - 1].x,
  //         cy: circleCoordinates[index - 1].y,
  //         d: rectCoordinates.length ? rectCoordinates[index - 1] : "",
  //       });
  //       timeout_ref.current = setTimeout(() => {
  //         setIndex((prevIndex) => prevIndex + 1);
  //       }, duration);
  //     }
  //     return () => {
  //       timeout_ref.current && clearTimeout(timeout_ref.current);
  //     };
  //   }, [index]);

  return (
    <>
      <animated.circle
        r="10"
        fill={color}
        cx={animatedProps.cx}
        cy={animatedProps.cy}
      />
      {animatedProps.d && (
        <animated.path
          d={animatedProps.d}
          stroke={color}
          strokeWidth={2}
          fill="none"
        />
      )}
    </>
  );
};

export default function SpringAnimations(props) {
  return (
    <animated.svg width={"1200"} height="1000">
      {coordinates.map((item, i) => (
        <SpringAnimateItem
          key={i}
          circleCoordinates={item}
          rectCoordinates={rectangles[i]}
          color={i % 2 === 0 ? "red" : "green"}
        />
      ))}
    </animated.svg>
  );
}
