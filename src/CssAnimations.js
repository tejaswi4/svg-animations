import React, { useEffect, useRef, useState } from "react";
const { coordinates } = require("./coordinates.json");
const { rectangles } = require("./rectangles.json");
const { connectors } = require("./connectors.json");
export const duration = 1000;

const Circles = (props) => {
  const { circleCoordinates, rectangleCoordinates = [], fill } = props;
  const coords = { x: circleCoordinates[0].x, y: circleCoordinates[0].y };
  const [position, setPosition] = useState(coords);
  const [rect, setRect] = useState(rectangleCoordinates[0]);
  const [index, setIndex] = useState(1);
  const timeout_ref = useRef();

  //   useEffect(() => {
  //     if (index >= circleCoordinates.length) {
  //       setIndex(1);
  //       setPosition(coords);
  //     }
  //   }, [index]);

  useEffect(() => {
    if (index < circleCoordinates.length) {
      const { x, y } = circleCoordinates[index];
      setPosition({ x, y });
      setRect(rectangleCoordinates[index]);
      timeout_ref.current = setTimeout(() => {
        setIndex((prevIndex) => prevIndex + 1);
      }, duration);
    }
    return () => {
      timeout_ref.current && clearTimeout(timeout_ref.current);
    };
  }, [index]);
  return (
    <>
      <circle
        r="10"
        fill={fill}
        cx={position.x}
        cy={position.y}
        className="circle-animation"
      />
      {!!rect && (
        <path
          d={rect}
          stroke={fill}
          strokeWidth={2}
          fill="none"
          className="rectangle-animation"
        />
      )}
    </>
  );
};

export default function CssAnimations(props) {
  const [lineIndex, setLineIndex] = useState(1);
  const connector_timeout_ref = useRef();
  const [line, setLine] = useState(connectors[0]);

  useEffect(() => {
    if (lineIndex < connectors.length) {
      setLine(connectors[lineIndex]);
      connector_timeout_ref.current = setTimeout(() => {
        setLineIndex((prevIndex) => prevIndex + 1);
      }, duration);
    }
    return () => {
      connector_timeout_ref.current &&
        clearTimeout(connector_timeout_ref.current);
    };
  }, [lineIndex]);
  return (
    <svg width={"1200"} height="1000">
      {coordinates.map((item, index) => (
        <Circles
          key={index}
          circleCoordinates={item}
          rectangleCoordinates={rectangles[index]}
          fill={index % 2 === 0 ? "red" : "green"}
        />
      ))}
      <path
        d={`M ${line.start.x} ${line.start.y} L ${line.end.x} ${line.end.y}`}
        stroke={line.start.lineColor || line.end.lineColor || "yellow"}
        strokeWidth="2"
        className="connector-animation"
      />
    </svg>
  );
}
