import React from "react";
import "./App.css";
import CssAnimations from "./CssAnimations";
import SvgAnimations from "./SvgAnimations";
import { useSetRecoilState } from "recoil";
import { playState } from "./recoilState";
import SpringAnimations from "./SpringAnimations";

function App() {
  const setIsPlaying = useSetRecoilState(playState);

  return (
    <div>
      <button
        onClick={() => {
          setIsPlaying((isPlaying) => !isPlaying);
        }}
      >
        Toggle Button
      </button>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-around",
          margin: 40,
        }}
      >
        <div>
          <h4>CSS Transition</h4>
          <CssAnimations />
        </div>
        <div>
          <h4>Svg Animation</h4>
          <SvgAnimations />
        </div>

        <div>
          <h4>React Spring</h4>
          <SpringAnimations />
        </div>
      </div>
    </div>
  );
}

export default App;
