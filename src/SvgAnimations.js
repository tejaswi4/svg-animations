import React, { createRef, useEffect, useRef } from "react";
import { useRecoilValue } from "recoil";
// import { duration } from "./CssAnimations";
import { playState } from "./recoilState";
const { coordinates } = require("./coordinates.json");
const { rectangles } = require("./rectangles.json");
const { connectors } = require("./connectors.json");

const duration = 5000;

const SvgAnimateItem = (props) => {
  const {
    pathCoordinates,
    circleRef,
    rectRef,
    rectanglePaths = [],
    color,
    ind,
    cx,
    cy,
  } = props;
  const dur = pathCoordinates.reduce((prev, curr) => prev + curr.duration, 0);
  const linePath = pathCoordinates.reduce((prev, curr, ind) => {
    if (ind === 0) {
      return `M${curr.x},${curr.y}`;
    } else {
      return `${prev} L${curr.x},${curr.y}`;
    }
  }, "");
  const circlePath = pathCoordinates.reduce((prev, curr, ind) => {
    const x = curr.x > cx ? curr.x - cx : cx - curr.x;
    const y = curr.y > cy ? curr.y - cy : cy - curr.y;
    return `${prev} L${x},${y}`;
  }, "M0, 0");

  const totalLength = pathCoordinates.length;
  const times = pathCoordinates
    .reduce((prev, curr, ind) => {
      if (ind === 0) {
        return [0];
      } else {
        return [...prev, prev[prev.length - 1] + curr.duration / dur];
      }
    }, [])
    .join("; ");

  const points = new Array(totalLength - 1).fill(1 / (totalLength - 1));
  const keyPoints = points
    .reduce(
      (prev, curr, ind) => {
        return [...prev, prev[prev.length - 1] + curr];
      },
      [0]
    )
    .join("; ");

  console.log("===keypoints", keyPoints, times)

  return (
    <>
      <path
        id={`motionPath_${ind}`}
        fill="none"
        stroke={color}
        strokeMiterlimit="10"
        d={linePath}
      />

      {pathCoordinates.map((item, i) => (
        <circle r="5" cx={item.x} cy={item.y} fill="blue" />
      ))}

      <circle id="circle" r="10" cx={cx} cy={cy} fill={color}>
        <animateMotion
          dur={`${dur}ms`}
          begin="indefinite"
          fill="freeze"
          keyPoints={keyPoints}
          keyTimes={times}
          // repeatCount="indefinite"
          ref={circleRef}
          path={circlePath}
          id={`circle_${ind}`}
          calcMode="linear"
        ></animateMotion>
      </circle>
      {rectanglePaths.length && (
        <path stroke={color} fill="none" strokeWidth={2}>
          <animate
            attributeName="d"
            begin={`circle_${ind}.begin`}
            dur={`${dur}ms`}
            fill="freeze"
            // repeatCount="indefinite"
            values={rectanglePaths.join(";")}
            ref={rectRef}
          />
        </path>
      )}
    </>
  );
};

export default function SvgAnimations(props) {
  // const coords = { cx: coordinates[0].x, cy: coordinates[0].y };
  const svgRef = useRef(null);
  const circleRefs = useRef([]);
  circleRefs.current = coordinates.map(
    (ele, i) => circleRefs.current[i] ?? createRef()
  );
  const rectRefs = useRef([]);
  rectRefs.current = rectangles.map(
    (ele, i) => rectRefs.current[i] ?? createRef()
  );
  const isPlaying = useRecoilValue(playState);

  useEffect(() => {
    isPlaying
      ? svgRef?.current?.unpauseAnimations()
      : svgRef?.current?.pauseAnimations();
  }, [isPlaying]);

  const startAnimation = () => {
    coordinates.forEach((item, i) => {
      circleRefs.current[i]?.current?.beginElement();
    });
    // rectangles.forEach((item, i) => {
    //   rectRefs.current[i]?.current?.beginElement();
    // });
  };

  return (
    <>
      <div>
        <button onClick={startAnimation}>Start</button>
      </div>
      <svg style={{ width: "1200", height: "1000" }} ref={svgRef}>
        {coordinates.map((item, index) => (
          <SvgAnimateItem
            key={index}
            pathCoordinates={item}
            rectanglePaths={rectangles[index]}
            circleRef={circleRefs.current[index]}
            rectRef={rectRefs.current[index]}
            color={index % 2 === 0 ? "red" : "green"}
            ind={index}
            cx={index % 2 === 0 ? "477.0000071633064" : "445.1422035764002"}
            cy={index % 2 === 0 ? "588.5000289026173" : "713.500011550846"}
          />
        ))}
        <path stroke={"yellow"} strokeWidth="2">
          <animate
            attributeName="d"
            values={connectors
              .map(
                (line) =>
                  `M ${line.start.x} ${line.start.y} L ${line.end.x} ${line.end.y}`
              )
              .join(";")}
            begin="indefinite"
            dur={"400ms"}
            // repeatCount="indefinite"
          />
        </path>
      </svg>
    </>
  );
}
